FROM debian:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
	python3 \
	cron \
	wget


RUN apt install -y tzdata && \
	ln -fs /usr/share/zoneinfo/Europe/Brussels /etc/localtime && \
	dpkg-reconfigure --frontend noninteractive tzdata


ENV TERM=linux
