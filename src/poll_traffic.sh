#!/bin/bash

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")


distance=$(wget --no-check-certificate -q -O - https://map-viewer-touring-mobilis.be-mobile.biz/service/be/trafficlength | grep -P -o "\"current\":\d*" | sed "s/.*://g" )


if [ ! -z $distance ]
then
	distance="$((${distance}/1000))"
	echo "$(date +%H:%M) ${distance}" >> $SCRIPT_DIR/data
fi
