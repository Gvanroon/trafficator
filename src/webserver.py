#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from parameters import *


class Handler(BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-Type','text/html')
        self.end_headers()

    def do_GET(self):
        print("### responding to GET")
        self.send_response(200)
        self.send_header('Content-Type','text/html')
        self.end_headers()
        
        dataFile = open("data.py","r")
        datacontent = dataFile.readlines()
        result = ""
        for line in datacontent:
            result = result + line.replace(" ", "&nbsp;") + "</br>"
        dataFile.close()
        content = '''
                    <html><head><meta http-equiv="refresh" content="5"></meta><title>Trafficator</title></head>
                    <body>
                    <text style="font-family:monospace;">{t}</text>
                    </body></html>
                    '''.format(t=result)

        #for line in content:
        self.wfile.write(bytes(content, 'UTF-8'))

if __name__ == '__main__':
    try:
        print("### starting server...")
        server = HTTPServer((HOST, PORT), Handler)
        server.serve_forever()

    except KeyboardInterrupt:
        print("### stopping server...")
        server.socket.close()
